# Factory.define :user do |user|
# 	user.name					"Bill Gates"
# 	user.email					"bill.gates@microsoft.com"
# 	user.password 				"billyjean"
# 	user.password_confirmation	"billyjean"
# end

FactoryGirl.define do 
  factory :user do
    name					"Bill Gates"
	email					"bill.gates@microsoft.com"
	password 				"billyjean"
	password_confirmation	"billyjean"
  end
end

FactoryGirl.define do
  sequence :email do |n|
	"person#{n}@marked.com"
  end
end

FactoryGirl.define do 
  factory :micropost do
    content		"Bill Gates"
	association	:user
  end
end
